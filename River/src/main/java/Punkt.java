
public class Punkt {
	private float mX;
	private float mY;

	public Punkt(float X, float Y) {

		this.setX(X);
		this.setY(Y);
	}

	public float getX() {
		return mX;
	}

	public void setX(float x) {
		this.mX = x;
	}

	public float getY() {
		return mY;
	}

	public void setY(float y) {
		this.mY = y;
	}

}
